## Download

To run the program, you must download nodejs and mongodb.

## How to run:
First start mongo
mongod --config /usr/local/etc/mongod.conf

### Start Backend
cd notefly
cd backend
npm install
node server

### Start Frontend
cd notely
npm install
sudo npm run electron-dev
