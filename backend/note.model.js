const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Note = new Schema({
    note_id: {
        id: String
    },
    title: {
        type: String
    },
    text: {
        type: String
    },
    owner: {
        type: String
    },
    tags: {
        type: Array
    },
    created: {
        type: Date
    },
    edited: {
        type: Date
    }
});

module.exports = mongoose.model('Note', Note);