const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const noteRoutes = express.Router();
const folderRoutes = express.Router();
const userRoutes = express.Router();
const PORT = 4000;

let User = require('./user.model');
var UsersRoutes = require('./user.routes');

app.use(cors());
app.use(bodyParser.json());

mongoose.connect('mongodb://127.0.0.1:27017/notefly', { useNewUrlParser: true });
const connection = mongoose.connection;

connection.once('open', function () {
    console.log("MongoDB database connection established successfully");
})

noteRoutes.route('/').get(function (req, res) {
    Note.find(function (err, notes) {
        if (err) {
            console.log(err);
        } else {
            res.json(notes);
        }
    });
});

noteRoutes.route('/owner/:owner').get(function (req, res) {
    let owner = req.params.owner;
    let owner_notes = [];
    Note.find(function (err, notes) {
        notes.filter(function (value, index, arr) {
            console.log(value);
            if (owner == value.owner)
                owner_notes.push(value);
        });

        if (err) {
            console.log(err);
        } else {
            res.json(owner_notes);
        }
    });
});

noteRoutes.route('/:id').get(function (req, res) {
    let id = req.params.id;
    Note.findById(id, function (err, note) {
        res.json(note);
    });
});

noteRoutes.route('/update/:id').post(function (req, res) {
    Note.findById(req.params.id, function (err, note) {
        if (!note)
            res.status(404).send("data is not found");
        else {
            note.title = req.body.title;
            note.text = req.body.text;
            note.edited = new Date();
            note.tags = req.body.tags;

            console.log(note);

            note.save().then(note => {
                res.json('Note updated!');
            })
                .catch(err => {
                    res.status(400).send("Update not possible");
                });
        }
    });
});

noteRoutes.route('/add').post(function (req, res) {
    let note = new Note(req.body);
    note.save()
        .then(note => {
            res.json(note)
        })
        .catch(err => {
            res.status(400).send('adding new note failed');
        });
});

noteRoutes.route('/delete/:id').post(function (req, res) {
    let id = req.params.id;
    console.log(id);
    Note.findByIdAndRemove(id, (err, note) => {
        if (err) return res.status(500).send(err);
        const response = {
            message: "Note successfully deleted",
            note_id: id
        };
        return res.status(200).send(response);
    });
});

app.use('/notes', noteRoutes);
app.use('/folders', folderRoutes);
app.use('/users', UsersRoutes);

app.listen(PORT, function () {
    console.log("Server is running on Port: " + PORT);
});