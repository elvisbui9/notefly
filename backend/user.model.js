const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserInfo = new Schema({
    first_name: {
        type: 'String'
    },
    last_name: {
        type: 'String'
    },
    email: {
        type: 'String',
        required: true
    },
    password: {
        type: 'String',
        required: true
    }
});

module.exports = User = mongoose.model('users', UserInfo); // users in video