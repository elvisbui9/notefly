const express = require('express');
const users = express.Router();
const cors = require('cors');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

process.env.SECRET_KEY = 'secret';

const User = require('./user.model');
users.use(cors());

users.post('/create_account', (req, res) => {
    const loginInfo = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        password: req.body.password
    }

    User.findOne({
        email: req.body.email
    })
        .then(user => {
            if (!user) {
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    loginInfo.password = hash
                    User.create(loginInfo)
                        .then(user => {
                            res.json({ status: user.email + ' registered!' })
                        })
                        .catch(err => {
                            res.send('Error: ' + err)
                        })
                })
            }
            else {
                res.json('User exists!');
            }
        })
        .catch(err => {
            res.send('Error: ' + err);
        })
})

users.post('/login_account', (req, res) => {
    User.findOne({
        email: req.body.email
    })
        .then(user => {
            if (user) {
                if (bcrypt.compareSync(req.body.password, user.password)) {
                    const infoLogin = {
                        id: user.id,
                        first_name: user.first_name,
                        last_name: user.last_name,
                        email: user.email
                    }
                    let token = jwt.sign(infoLogin, process.env.SECRET_KEY, {
                        expiresIn: 2550
                    })
                    res.send(token)
                }
                else {
                    res.json({ error: "Wrong Password! " })
                }
            }
            else {
                res.json({ error: "User Email doesn't exist! " })
            }
        })
        .catch(err => {
            res.send('Error: ' + err);
        })
})

users.get('/profile', (req, res) => {
    var decoded = jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)

    User.findOne({
        id: decoded.id
    })
        .then(user => {
            if (user) {
                res.json(user)
            }
            else {
                res.send("User doesn't exist!")
            }
        })
        .catch(err => {
            res.send('Error: ' + err);
        })
})

module.exports = users;