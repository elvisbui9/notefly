import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import {
  Navbar,
  Nav,
  NavDropdown,
  Form,
  FormControl,
  Button
} from "react-bootstrap";
import logo from "./logo.svg";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import { Note, Folder, Store } from "./util/store.js";
import axios from 'axios';
import NotesListings from './components/notes-listing.component';
import NoteField from "./components/note-field.component";
import NewNoteField from "./components/new-note.component";
import ListGroup from 'react-bootstrap/ListGroup';
import "./components/Folder.css";
import LoginAccount from "./components/login-account.component.js";
import CreateAccount from "./components/create-account.component.js";
import LogOutAccount from "./components/logout.component.js";
import Home from "./components/home.component.js";
import UserProfile from "./components/userProfile.component.js";
import NoteChart from "./components/chart.component.js";
import NoteHeatMap from "./components/heatmap.component.js";
import TagBubbleGraph from "./components/bubble-graph.component.js";

class App extends Component {

  constructor(props) {
    super(props);
    this.get_nav_bar_contents = this.get_nav_bar_contents.bind(this)
    this.get_login = this.get_login.bind(this)
    this.charts = this.charts.bind(this)
    this.bubble_graph = this.bubble_graph.bind(this)
    this.state = { token: localStorage.usertoken };
  }

  get_nav_bar_contents() {
    if (this.state.token) {
      return (
        <li className="navbar-item">
          <Link to="/newnote" className="nav-link">New Note</Link>
        </li>
      )
    }
    return (
      <li className="navbar-item">
        <Link to="/create" className="nav-link">Create Account</Link>
      </li>
    )
  }

  get_login() {
    if (this.state.token) {
      return (
        <Link to="/logout" className="nav-link">
          <div className="btn btn-primary" >Logout</div>
        </Link>
      )
    }
    return (
      <Link to="/login" className="nav-link">
        <div className="btn btn-primary" >Login</div>
      </Link>
    )
  }

  charts() {
    if (this.state.token) {
      return (
        <li className="navbar-item">
          <Link to="/heatmap" className="nav-link">Heatmap</Link>
        </li>
      )
    }
  }

  bubble_graph() {
    if (this.state.token) {
      return (
        <li className="navbar-item">
          <Link to="/bubble" className="nav-link">Bubble Graph</Link>
        </li>
      )
    }
  }

  render() {
    return (
      <Router>
        <nav className="navbar navbar-expand-sm rounded">
          <div className="collapse navbar-collapse text-center">
            <ul className="navbar-nav mr-auto">
              <li className="navbar-item">
                <Link to="/" className="nav-link">Home</Link>
              </li>
              {this.get_nav_bar_contents()}
              {this.charts()}
              {this.bubble_graph()}
            </ul>
          </div>
          <div className="navbar-collapse collapse w-100 order-3 dual-collapse2">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <div className="form-group" align>
                  {this.get_login()}
                </div>
              </li>
            </ul>
          </div>
        </nav>
        <Route path="/" exact component={Home} />
        <Route path="/login" exact component={LoginAccount} />
        <Route path="/create" exact component={CreateAccount} />
        <Route path="/profile" exact component={UserProfile} />
        <Route path="/note/:id" component={NoteField} />
        <Route path="/newnote" component={NewNoteField} />
        <Route path="/chart/:id" component={NoteChart} />
        <Route path="/heatmap" component={NoteHeatMap} />
        <Route path="/logout" component={LogOutAccount} />
        <Route path="/bubble" component={TagBubbleGraph} />
      </Router>
    );
  }
}

export default App;
