// function to remove extra white spaces before and after a string
function remove_spaces(str) {
  var newStr = str.replace(/^\s+|\s+$/gm,'');
  console.log(newStr);
}

// test the above function with a few strings - add html strings as well
remove_spaces("     hello bro!    ")

// replace between, after, and before tags - whitespaces
function remove_tag_spaces(str) {
  var newStr1 = str.replace(/[\t ]+\</g, "><");
  console.log(newStr1);
}

// random test - doesn't work well
var htmlString= "<div> <h1>     Hello World     </h1> </div>";
remove_tag_spaces(htmlString);

// function to bold a string
function bold_text(str) {
  var boldStr = str.bold();
  console.log(boldStr);
}

// test out bold string
bold_text("Hello World!");

// fucntion to italics a string
function italics_string(str) {
  var italicsStr = str.italics();
  console.log(italicsStr);
}

// test italics string
italics_string("Hello!");

// function to change the colors fo a text
function text_color_change(str) {
  var fontColorStr = str.fontcolor("purple");
  console.log(fontColorStr);
}

// test font color
text_color_change("Hello user!");

// highlight a string
function highlight_string(str) {
  console.log('<mark>' + str + '</mark>')
}

// test highlight
highlight_string("Gorillas!");

// underline a string
function underline_string(str) {
  console.log('<u>' + str + '</u>');
}

// test underline
underline_string("here i am");

// add a list of bullets
function bullets_list(array, str) {
  str += '<ul>';

  for (var i = 0; i < array.length; i++) {
    str += ('<li>'+array[i]+'</li>');
  }

  str += '<ul>';

  console.log(str);
}

// test bullet list
var array = ["apples", "chicken", "icecream"];

bullets_list(array, "Food:");
