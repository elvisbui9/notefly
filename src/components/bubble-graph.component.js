import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Note, Folder, Store } from "../util/store.js";
import axios from 'axios';
import HeatMap from 'react-heatmap-grid';
import BubbleChart from '@weknow/react-bubble-chart-d3';
const jwtd = require('jwt-decode');

const xLabels = new Array(24).fill(0).map((_, i) => `${i}`);
const yLabels = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
const data = new Array(yLabels.length)
    .fill(0)
    .map(() => new Array(xLabels.length).fill(0).map(() => Math.floor(Math.random() * 100)));


export default class Home extends Component {

    constructor(props) {
        super(props);
        this.state = { data: [], words: [] };
    }
    componentDidMount() {
        const token = localStorage.usertoken;
        if (token) {
            const decoded = jwtd(token);
            console.log('decode ', decoded);
            axios.get('http://localhost:4000/notes/owner/' + decoded.email)
                .then(response => {
                    console.log(response.data)
                    this.setState({ notes: response.data, all: response.data })
                    let dict = {};
                    this.state.notes.map(function (currentNote, i) {
                        currentNote.tags.map(function (t, ti) {
                            if (dict[t]) {
                                dict[t] += 1
                            } else {
                                dict[t] = 1
                            }
                        })
                    })
                    let result = []
                    for (let p in dict) {
                        result.push({ label: p, value: dict[p] })
                    }
                    this.setState({ data: result })
                })
                .catch(function (error) {
                    console.log(error);
                })
        }
    }

    render() {
        return (
            <div>
                <h3>Tags Bubble Graph</h3>
                <BubbleChart
                    graph={{
                        zoom: 1.1,
                        offsetX: -0.05,
                        offsetY: -0.01,
                    }}
                    width={1000}
                    height={800}
                    padding={0} // optional value, number that set the padding between bubbles
                    showLegend={true} // optional value, pass false to disable the legend.
                    legendPercentage={20} // number that represent the % of with that legend going to use.
                    legendFont={{
                        family: 'Arial',
                        size: 12,
                        color: '#000',
                        weight: 'bold',
                    }}
                    valueFont={{
                        family: 'Arial',
                        size: 12,
                        color: '#fff',
                        weight: 'bold',
                    }}
                    labelFont={{
                        family: 'Arial',
                        size: 16,
                        color: '#fff',
                        weight: 'bold',
                    }}
                    //Custom bubble/legend click functions such as searching using the label, redirecting to other page
                    bubbleClickFunc={this.bubbleClick}
                    legendClickFun={this.legendClick}
                    data={this.state.data}
                />
            </div>
        )
    }
}