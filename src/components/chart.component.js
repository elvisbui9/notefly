import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Note, Folder, Store } from "../util/store.js";
import axios from 'axios';
import ReactWordcloud from 'react-wordcloud';
import words from './words';

const banned_words = ["we", "the", "and", "for", "if", "of", "a", "in", "on", "to", "for", "that", "so", "this", "from", "are", "is", "with", "their", "it", "can", "like", "as", "also", "by", "not", "or", "be", "such", "but", "an", "they"];

export default class Home extends Component {

  constructor(props) {
    super(props);
    this.state = { data: [], words: [] };
  }

  componentWillMount() {
    if (this.props.match.params.id) {
      axios.get('http://localhost:4000/notes/' + this.props.match.params.id)
        .then(response => {
          this.setState({ note: response.data });
          console.log(this.state.note.text);
          let str = this.state.note.text;
          str = str.toUpperCase();
          let res = str.split(" ");
          let counts = {};
          let words = [];
          res.forEach(
            function (s) {
              counts[s] = (counts[s] || 0) + 1;
            }
          )

          for (let prop in counts) {
            if(!banned_words.includes(prop.toLowerCase()))
              words.push({ text: prop, value: counts[prop] })
          }
          this.setState({ words: words });
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  }

  render() {
    return (
      <div>
        <ReactWordcloud words={this.state.words} />
      </div>
    )
  }
}
