import React, { Component } from 'react';
import axios from 'axios';

export const register = newUser => {
  return axios
    .post('users/create_account', {
      first_name: newUser.first_name,
      last_name: newUser.last_name,
      email: newUser.email,
      password: newUser.password,
    })
    .then(res => {
      console.log("Account Created!");
    })
}

export default class CreateAccount extends Component {
  constructor(props) {
    super(props);

    this.onChangeFirstName = this.onChangeFirstName.bind(this);
    this.onChangeLastName = this.onChangeLastName.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      first_name: '',
      last_name: '',
      email: '',
      password: ''
    }
  }

  onChangeFirstName(e) {
    this.setState({
      first_name: e.target.value
    })
  }

  onChangeLastName(e) {
    this.setState({
      last_name: e.target.value
    })
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value
    })
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    })
  }

  onSubmit(e) {
    e.preventDefault();

    console.log(`Form Submitted: `);
    console.log(`First Name: ${this.state.first_name}`);
    console.log(`Last Name: ${this.state.last_name}`);
    console.log(`Email: ${this.state.email}`);
    console.log(`Password: ${this.state.password}`);

    this.setState({
      first_name: '',
      last_name: '',
      email: '',
      password: ''
    })

    const newUser = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      email: this.state.email,
      password: this.state.password
    }

    /*register(newUser).then(res => {
      this.props.history.push('/login');
    })*/

    return axios
      .post('http://localhost:4000/users/create_account', newUser)
      .then(res => {
        console.log("Account Created!");
        this.props.history.push('/login');
      })
  }

  render() {
    return (
      <body>
        <div className="container" align="center">
          <div className="page-header" style={{ margin: 120 }}>
            <div className="col-sm-6 mx-atuo">
              <h3>Create Account</h3>
              <div class="border" align="center"></div>
              <div style={{ marginTop: 20 }}>
                <form onSubmit={this.onSubmit}>
                  <div className="form-group">
                    <label>First Name</label>
                    <input type="text"
                      className="form-control"
                      value={this.state.first_name}
                      onChange={this.onChangeFirstName}
                    />
                  </div>
                  <div className="form-group">
                    <label>Last Name</label>
                    <input type="text"
                      className="form-control"
                      value={this.state.last_name}
                      onChange={this.onChangeLastName}
                    />
                  </div>
                  <div className="form-group">
                    <label>Email</label>
                    <input type="text"
                      className="form-control"
                      value={this.state.email}
                      onChange={this.onChangeEmail}
                    />
                  </div>
                  <div className="form-group">
                    <label>Password</label>
                    <input type="password"
                      className="form-control"
                      value={this.state.password}
                      onChange={this.onChangePassword}
                    />
                  </div>
                  <div className="form-group">
                    <input type="submit" value="Create Account" className="btn btn-primary" />
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </body>
    )
  }
}
