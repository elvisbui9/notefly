import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Form, Button, ButtonToolbar } from 'react-bootstrap';
import "./Edit.css";
import Dropdown from 'react-bootstrap/Dropdown'

export default class Edit extends Component {
	constructor(props) {
		super(props);
		this.onNewNote = this.onNewNote.bind(this);

		this.state = {
			folders: this.props.folderList,
			notes: this.props.noteList,
			current: this.props.current
		}
		console.log(this.state.folders);
		console.log(this.state.notes);

	}

	render() {
		return (
			<div id="edit-div" class='bg-dark'>
				<Form.Group id="folder">
					<Form.Control as="select">
						{this.state.folders.map(note => (
							<option>
								{note.getTitle()}
							</option>
						))}
					</Form.Control>
				</Form.Group>

				<button id='new-note' onClick={this.onNewNote} class='btn-dark button.sharp'> New Note </button>
			</div>
		);
	}
}
