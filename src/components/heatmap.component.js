import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Note, Folder, Store } from "../util/store.js";
import axios from 'axios';
import HeatMap from 'react-heatmap-grid';
const jwtd = require('jwt-decode');

const xLabels = new Array(24).fill(0).map((_, i) => `${i}`);
const yLabels = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
const data = new Array(yLabels.length)
    .fill(0)
    .map(() => new Array(xLabels.length).fill(0).map(() => Math.floor(Math.random() * 100)));


export default class Home extends Component {

    constructor(props) {
        console.log(new Array(xLabels.length).fill(0).map(() => Math.floor(Math.random() * 100)))
        super(props);
        let dict = new Array(7)
            .fill(0)
            .map(() => new Array(24).fill(0).map(() => 0));
        this.state = { data: dict, words: [] };
    }
    componentDidMount() {
        const token = localStorage.usertoken;
        if (token) {
            const decoded = jwtd(token);
            console.log('decode ', decoded);
            axios.get('http://localhost:4000/notes/owner/' + decoded.email)
                .then(response => {
                    console.log(response.data)
                    this.setState({ notes: response.data, all: response.data })
                    let dict = new Array(7)
                        .fill(0)
                        .map(() => new Array(24).fill(0).map(() => 0));
                    console.log(dict)
                    this.state.notes.map(function (currentNote, i) {
                        let created_day = new Date(currentNote.created).getDay()
                        let created_hour = new Date(currentNote.created).getHours()
                        dict[created_day][created_hour] += 1
                        let edited_day = new Date(currentNote.edited).getDay()
                        let edited_hour = new Date(currentNote.edited).getHours()
                        dict[edited_day][edited_hour] += 1
                    })
                    this.setState({ data: dict })
                })
                .catch(function (error) {
                    console.log(error);
                })
        }
    }

    render() {
        return (
            <div>
                <HeatMap
                    xLabels={xLabels}
                    yLabels={yLabels}
                    data={this.state.data}
                />
            </div>
        )
    }
}