import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Note, Folder, Store } from "../util/store.js";
import axios from 'axios';

const jwtd = require('jwt-decode');

const List = props => (
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">{props.list.title.substr(0, 100)}</h5>
        <p class="card-text">{props.list.text.substr(0, 100)} &nbsp;</p>
        <Link to={"/note/" + props.list._id}><a href="#" class="btn btn-primary">Edit</a></Link>
        <Link to={"/chart/" + props.list._id}><a href="#" class="btn btn-primary">Word Cloud</a></Link>
      </div>
    </div>
  </div>
);

export default class Home extends Component {

  constructor(props) {
    super(props);
    this.onChangeSearch = this.onChangeSearch.bind(this);
    this.state = { notes: [], search: '', folder: new Folder() };
  }

  componentDidMount() {
    const token = localStorage.usertoken;
    if (token) {
      const decoded = jwtd(token);
      console.log('decode ', decoded);
      axios.get('http://localhost:4000/notes/owner/' + decoded.email)
        .then(response => {
          this.setState({ notes: response.data, all: response.data })
        })
        .catch(function (error) {
          console.log(error);
        })
    }
  }

  onChangeSearch(e) {
    this.setState({
      search: e.target.value
    });
    let filteredListings = [];
    this.state.all.map(function (l, i) {
      if (l.title.includes(e.target.value) || l.text.includes(e.target.value) || l.tags.includes(e.target.value)) {
        filteredListings.push(l);
      }
    })

    this.setState({ notes: filteredListings });
  }

  notesList() {
    return this.state.notes.map(function (currentNote, i) {
      return (<List list={currentNote} key={i} />);
    })
  }

  render() {
    return (
      <div className="container">
        <div className="jumbotron mt-5">
          <div className="col-sm-8 mx-atuo">
            <h1>NOTEFLY!</h1>
          </div>
        </div>
        <div class="active-pink-3 active-pink-4 mb-4">
          <input value={this.state.search}
            onChange={this.onChangeSearch}
            class="form-control"
            type="text"
            placeholder="Search"
            aria-label="Search" />
        </div>
        <div class="row">
          {this.notesList()}
        </div>
      </div>
    )
  }
}
