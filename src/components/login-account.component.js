import React, { Component } from 'react';
import axios from 'axios';

export const login = user => {
  return axios
    .post('http://localhost:4000/users/login_account', {
      email: user.email,
      password: user.password,
    })
    .then(res => {
      localStorage.setItem('usertoken', res.data);
      return res.data;
    })
    .catch(err => {
      console.log(err);
    })
}

export default class LoginAccount extends Component {

  constructor(props) {
    super(props);

    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    //this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      email: '',
      password: '',
    }
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value
    })
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    })
  }

  onSubmit(e) {
    e.preventDefault();

    this.setState({
      email: '',
      password: '',
    })

    const user = {
      email: this.state.email,
      password: this.state.password
    }

    axios
      .post('http://localhost:4000/users/login_account', {
        email: user.email,
        password: user.password,
      })
      .then(res => {
        localStorage.setItem('usertoken', res.data);
        console.log(res)
        this.props.history.push('/profile');
      })
      .catch(err => {
        console.log(err);
      })
  }

  render() {
    return (
      <div className="container">
        <div className="jumbotron mt-5">
          <div className="col-sm-6 mx-atuo">
            <h1>NOTEFLY!</h1>
          </div>
        </div>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Email</label>
            <input type="text"
              className="form-control"
              value={this.state.email}
              onChange={this.onChangeEmail}
            />
          </div>
          <div className="form-group">
            <label>Password</label>
            <input type="password"
              className="form-control"
              value={this.state.password}
              onChange={this.onChangePassword}
            />
          </div>
          <div className="form-group">
            <input type="submit" value="Login" className="btn btn-primary" />
          </div>
        </form>

      </div>
    )
  }
}