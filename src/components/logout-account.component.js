import React, { Component } from 'react';

export default class LogoutAccount extends Component {
  render() {
    return (
      <div className="container" align="center">
        <div className="col-sm-8 mx-atuo">
          <p><font size="36">Click button to Logout!</font></p>
        </div>
        <div className="form-group">
          <input type="submit" value="Logout" className="btn btn-primary" />
        </div>
      </div>
    )
  }
}