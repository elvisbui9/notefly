import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Form, Button, ButtonToolbar } from 'react-bootstrap';
// import axios from 'axios';
// import queryString from "query-string";
import "./Note.css";
import { Note, Folder, Store } from "../util/store.js";
import axios from 'axios';
const localStorage = window.localStorage;
const jwtd = require('jwt-decode');


export default class LoginGoogle extends Component {

    constructor(props) {
        super(props);
    }

    // performs action after page is loaded
    componentDidMount() {
        if (localStorage.usertoken) {
            localStorage.removeItem('usertoken');
            localStorage.removeItem('loggedin');
            window.location.reload();
        } else {
            this.props.history.push('/');
        }
    }

    render() {
        return (
            <div >
                Logging out...
            </div>
        )
    }

}