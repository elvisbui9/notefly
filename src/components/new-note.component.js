import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Form, Button, ButtonToolbar } from 'react-bootstrap';
// import axios from 'axios';
// import queryString from "query-string";
import "./Note.css";
import { Note, Folder, Store } from "../util/store.js";
import axios from 'axios';
const localStorage = window.localStorage;
const jwtd = require('jwt-decode');


export default class LoginGoogle extends Component {

    constructor(props) {
        super(props);
    }

    // performs action after page is loaded
    componentDidMount() {
        const token = localStorage.usertoken;
        if(token)
        {
            const decoded = jwtd(token);
            console.log('decode ', decoded);
            let newNote = new Note();
            newNote.owner = decoded.email;
            axios.post('http://localhost:4000/notes/add', newNote)
                .then(res => this.props.history.push('/note/' + res.data._id));
        }   
    }

    render() {
        return (
            <div >
                Creating...
            </div>
        )
    }

}