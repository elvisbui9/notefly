import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Form, Button, ButtonToolbar } from 'react-bootstrap';
// import axios from 'axios';
// import queryString from "query-string";
import "./Note.css";
import { Note, Folder, Store } from "../util/store.js";
import axios from 'axios';
import ReactDOM from 'react-dom';
const localStorage = window.localStorage;

export default class NoteField extends Component {

  constructor(props) {
    super(props);

    //always do this to all methods you want to use in render()
    this.onChangeNote = this.onChangeNote.bind(this);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeTags = this.onChangeTags.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.removeSpaces = this.removeSpaces.bind(this)
    this.capFirstLetters = this.capFirstLetters.bind(this)
    this.state = {
      note: new Note(),
      folders: []
    };
  }

  onChangeNote(e) {
    let n = this.state.note;
    n.text = e.target.value;
    this.setState({ note: n })
    axios.post('http://localhost:4000/notes/update/' + this.props.match.params.id, this.state.note)
      .then(res => console.log(res.data));
  }

  onChangeTitle(e) {
    var n = this.state.note;
    n.title = e.target.value
    this.setState({ note: n })
    axios.post('http://localhost:4000/notes/update/' + this.props.match.params.id, this.state.note)
      .then(res => console.log(res.data));
  }

  onSubmit(e) {
    e.preventDefault();
    axios.post('http://localhost:4000/notes/delete/' + this.state.note._id)
      .then(res => console.log(res.data));
    this.props.history.push('/');
  }

  onChangeTags(e) {
    var n = this.state.note;
    var tags_str = e.target.value.replace(" ", ",");
    document.getElementById('tags').value = tags_str;
    n.tags = document.getElementById('tags').value.split(",");
    console.log(this.state.note);
    this.setState({ note: n });
    axios.post('http://localhost:4000/notes/update/' + this.state.note._id, this.state.note)
      .then(res => console.log(res.data));
  }

  // performs action before page is loaded
  componentWillMount() {
    if (this.props.match.params.id) {
      axios.get('http://localhost:4000/notes/' + this.props.match.params.id)
        .then(response => {
          this.setState({ note: response.data });
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  }

  // performs action after page is loaded
  componentDidMount() {


  }

  removeSpaces(e) {
    e.preventDefault();
    let n = this.state.note;
    n.text = n.text.replace(/\s+/g, ' ').trim();
    this.setState({ note: n })
    axios.post('http://localhost:4000/notes/update/' + this.props.match.params.id, this.state.note)
      .then(res => console.log(res.data));
  }

  capFirstLetters(e) {
    e.preventDefault();
    let n = this.state.note;
    n.text = n.text.split('.').map(function (x) {
      x = x.trim();
      return x.substr(0, 1).toUpperCase() + x.substr(1);
    }).join('. ');
    this.setState({ note: n })
    axios.post('http://localhost:4000/notes/update/' + this.props.match.params.id, this.state.note)
      .then(res => console.log(res.data));
  }

  render() {
    return (
      <div className="container">
        <div className="page-header">
          <div className="col-sm-6 mx-atuo" style={{ marginTop: 20 }}>
            <h1>Notes</h1>
            <div class="about-border" align="left"></div>
          </div>
        </div>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Title</label>
            <input type="text"
              id="title"
              className="form-control"
              value={this.state.note.title}
              onChange={this.onChangeTitle}
              placeholder="Untitled"
            //class = "button.sharp"
            />
          </div>
          <div className="form-group">
            <label>Notes</label>
            <textarea type="text"
              id="text-area"
              className="form-control"
              value={this.state.note.text}
              onChange={this.onChangeNote}
              rows="20"
            />
          </div>
          <div className="form-group">
            <input onClick={this.removeSpaces} type="submit" value="Remove Extra White Spaces" className="btn btn-primary" />
            <input onClick={this.capFirstLetters} type="submit" value="Capitalize First Letters" className="btn btn-primary" />
          </div>
          <div className="group" align="center">
            <label>Notes Auto Saved! :)</label>
          </div>
          <div className="form-group">
            <label>Tags</label>
            <input type="text"
              id="tags"
              className="form-control"
              value={this.state.note.tags}
              onChange={this.onChangeTags}
            />
          </div>
          <div className="form-group">
            <input type="submit" value="Delete Note!" className="btn btn-primary" />
          </div>
        </form>

      </div>
    )
  }

}
