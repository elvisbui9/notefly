import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Note, Folder, Store } from "../util/store.js";
import axios from 'axios';
const List = props => (
    <tr>
        <td >{props.list.title}</td>
        <td >{props.list.text}</td>
        <td>
            <Link to={"/note/" + props.list._id}>View</Link>
        </td>
    </tr>
)


export default class ViewNote extends Component {

    constructor(props) {
        super(props);
        this.onChangeSearch = this.onChangeSearch.bind(this);
        this.state = { notes: [], search: '', folder: new Folder() };
    }

    componentDidMount() {
        axios.get('http://localhost:4000/notes')
            .then(response => {
                this.setState({ notes: response.data, all: response.data })
            })
            .catch(function (error) {
                console.log(error);
            })
        if (this.props.match) {
            let folders = Store.get_folders();
            console.log(folders)
            console.log(folders[0].folder_id == this.props.match.params.id)
            let f = folders.find(x => x.folder_id == this.props.match.params.id)
            console.log(f);
            console.log(this.props.match.params.id);
            let filtered_notes = this.state.notes.filter(function (x) {
                console.log(x);
                return f.notes.includes(x.note_id)
            })
            this.setState({ notes: filtered_notes, all: filtered_notes, folder: f })
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.match && this.props.match && nextProps.match.params.id !== this.props.match.params.id) {
            let notes = Store.get_notes();
            if (nextProps.match) {
                let folders = Store.get_folders();
                console.log(folders)
                console.log(folders[0].folder_id == nextProps.match.params.id)
                let f = folders.find(x => x.folder_id == nextProps.match.params.id)
                let filtered_notes = notes.filter(function (x) {
                    return f.notes.includes(x.note_id)
                })
                this.setState({ notes: filtered_notes, all: filtered_notes, folder: f })
            } else {

                this.setState({ notes: notes, all: notes })
            }
        }
    }

    notesList() {
        return this.state.notes.map(function (currentNote, i) {
            return <List list={currentNote} key={i} />;
        })
    }

    onChangeSearch(e) {
        this.setState({
            search: e.target.value
        });
        let filteredListings = [];
        this.state.all.map(function (l, i) {
            if (l.title.includes(e.target.value) || l.text.includes(e.target.value)) {
                filteredListings.push(l);
            }
        })

        this.setState({ notes: filteredListings });

    }

    pageTitle(that) {
        if (that.props.match) {
            return <h3>{that.state.folder.title}</h3>
        } else {
            return <h3>Notes</h3>
        }
    }

    render() {
        return (
            <div>
                Notes
                <div class="active-pink-3 active-pink-4 mb-4">
                    <input value={this.state.search}
                        onChange={this.onChangeSearch} class="form-control" type="text" placeholder="Search" aria-label="Search" />
                </div>
                <table className="table table-striped" style={{ marginTop: 20 }} >
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Text</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.notesList()}
                    </tbody>
                </table>
            </div>
        )
    }

}