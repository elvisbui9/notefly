import React, { Component } from 'react';
const jwtd = require('jwt-decode');

export default class UserProfile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      first_name: '',
      last_name: '',
      email: '',
    }
  }

  componentDidMount() {
    if (localStorage.loggedin) {
      const token = localStorage.usertoken;
      const decoded = jwtd(token);
      console.log('decode ', decoded)
      this.setState({
        first_name: decoded.first_name,
        last_name: decoded.last_name,
        email: decoded.email
      })
    } else {
      localStorage.setItem('loggedin', true);
      window.location.reload();
    }
  }

  render() {
    return (
      <div className="container">
        <div className="jumbotron mt-3">
          <div className="col-sm-6 mx-atuo">
            <h1>NOTEFLY!</h1>
          </div>
          <form onSubmit={this.onSubmit}>
            <div className="form-group">
              <label>First Name</label>
              <input type="text"
                className="form-control"
                value={this.state.first_name}
              />
            </div>
            <div className="form-group">
              <label>Last Name</label>
              <input type="text"
                className="form-control"
                value={this.state.last_name}
              />
            </div>
            <div className="form-group">
              <label>Email</label>
              <input type="text"
                className="form-control"
                value={this.state.email}
              />
            </div>
          </form>
        </div>
      </div>
    )
  }
}