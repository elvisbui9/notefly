// function to remove extra white spaces
export const remove_white_spaces = (str) => {
  return str.replace(/\s+/g, " ").trim();
};

// function to bold text
export const bolding_text = (str) => {
  return str.bold();
}

// function to italicize string
export const italicize_text = (str) => {
  return str.italics();
}

// function to change color to a text
export const change_color_text = (str, color) => {
  return str.fontcolor(color);
}

// function to highlight text
export const highlight_text = (str) => {
  return '<mark>' + str + '</mark>';
}

// function to underline text
export const underline_text = (str) => {
  return '<u>' + str + '</u>';
}

// function to create a bulleted list
export const bulleted_list = (array, str) => {
  str += '<ul>';

  for (var i = 0; i < array.length; i++) {
    str += ('<li>'+array[i]+'</li>');
  }

  str += '<ul>';

  return str;
}
