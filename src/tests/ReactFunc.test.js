import { remove_white_spaces, bolding_text, italicize_text, change_color_text, highlight_text, underline_text, bulleted_list } from './ReactFunc';

// TESTING BEGINS

// remove whitespaces test
describe('remove_white_spaces', () => {
  it('should remove any extra white spaces in a string', () => {
    expect(remove_white_spaces("     hello     bro!    ")).toBe("hello bro!");
  })
});

// bold test
describe('bolding_text', () => {
  it('should bold a string', () => {
    expect(bolding_text("Hello World!")).toBe("<b>Hello World!</b>");
  })
});

// italics test
describe('italicize_text', () => {
  it('should italicize a string', () => {
    expect(italicize_text("Hello!")).toBe("<i>Hello!</i>");
  })
});

// make color changes test
describe('change_color_text', () => {
  it('should change the color of a string', () => {
    expect(change_color_text("Hello User!", "purple")).toBe("<font color=\"purple\">Hello User!</font>");
  })
});

// highlight test
describe('highlight_text', () => {
  it('should highlight a string', () => {
    expect(highlight_text("Gorillas!")).toBe("<mark>Gorillas!</mark>");
  })
});

// remove whitespaces test
describe('underline_text', () => {
  it('should underline a string', () => {
    expect(underline_text("here i am")).toBe("<u>here i am</u>");
  })
});

// remove whitespaces test
describe('bulleted_list', () => {
  it('should create a bulleted list', () => {
    expect(bulleted_list(["apples", "chicken", "icecream"], "Food:")).toBe("Food:<ul><li>apples</li><li>chicken</li><li>icecream</li><ul>");
  })
});
