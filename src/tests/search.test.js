import {searchWord} from "./search.js";

// TESTING BEGINS

// remove whitespaces test
describe('searchWord', () => {
  it('should search for a word in an array', () => {
    expect(searchWord("folder")).toBe(-1);
  })
});

describe('searchWord', () => {
  it('should search for a word in an array', () => {
    expect(searchWord("Folder 1")).toBe("Folder 1 found!");
  })
});
