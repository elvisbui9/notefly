import axios from 'axios';
let ls = window.localStorage;

function Note() {
  return {
    title: 'untitled',
    text: '',
    owner: '',
    tags: [],
    created: new Date(),
    edited: new Date(),
    note_id: new Date().valueOf() + Math.random()
  }
}

function Folder() {
  return {
    title: 'untitled',
    notes: [],
    created: new Date(),
    edited: new Date(),
    folder_id: new Date().valueOf() + Math.random()
  }
}

function get_notes() {
  let notes = [];
  axios.get('http://localhost:4000/notes')
    .then(response => {
      console.log('data', response.data)
      notes = response.data;
    })
    .catch(function (error) {
      console.log(error);
    })
  return notes;
}

function save_notes(notes) {
  ls.setItem('notes', JSON.stringify(notes))
}

function save_note(n) {
  let notes = get_notes();
  notes.push(n)
  ls.setItem('notes', JSON.stringify(notes))
}

function update_note(note) {
  let notes = get_notes();
  let i = notes.findIndex(function (elm) {
    return elm.id = note.note_id;
  })
  if (i === -1) {
    notes.push(note)
  } else {
    notes[i] = note;
  }
  save_notes(notes)
}

function delete_note(id) {
  let notes = get_notes();
  let i = notes.findIndex(function (elm) {
    return elm.note_id = id;
  })
  notes.splice(i, 1);
  save_notes(notes)
}

function get_folders() {
  let folders = [];
  if (ls.getItem('folders') !== null && ls.getItem('folders').length !== 0) {
    folders = JSON.parse(ls.getItem('folders'))
  }
  return folders;
}

function save_folders(notes) {
  ls.setItem('folders', JSON.stringify(notes))
}

function save_folder(f) {
  let folders = get_folders();
  folders.push(f)
  ls.setItem('folders', JSON.stringify(folders))
}


const Store = {
  get_notes: get_notes,
  save_notes: save_notes,
  save_note: save_note,
  update_note: update_note,
  delete_note: delete_note,
  get_folders: get_folders,
  save_folder: save_folder
}

export { Note, Folder, Store };